package uz.diamondsolutions.sushikatanaapp

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.widget.TextView
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import uz.diamondsolutions.sushikatanaapp.activity.CartActivity
import uz.diamondsolutions.sushikatanaapp.activity.LoginActivity
import uz.diamondsolutions.sushikatanaapp.activity.OrderHistoryActivity
import uz.diamondsolutions.sushikatanaapp.fragment.MainPagerFragment
import uz.diamondsolutions.sushikatanaapp.dialog.LogoutRequestDialog
import uz.diamondsolutions.sushikatanaapp.fragment.MealListFragment
import uz.diamondsolutions.sushikatanaapp.models.User
import uz.diamondsolutions.sushikatanaapp.utils.AppFirebaseIdService
import uz.diamondsolutions.sushikatanaapp.utils.RealmController
import uz.diamondsolutions.sushikatanaapp.utils.RequestCodes


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var token: String
    private lateinit var pref: SharedPreferences
    private lateinit var realm: Realm
    var user: User? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        pref = PreferenceManager.getDefaultSharedPreferences(this)
        realm = RealmController.with(this).realm
        token = pref.getString("TOKEN", "")!!
        Log.d("APP_USER==NULL", (user == null).toString())

        if (token === "") {
            val logIntent = Intent(this, LoginActivity::class.java)
            startActivityForResult(logIntent, RequestCodes.REQUEST_LOGIN)
        } else {
            loadActivityData()
        }

        fab.setOnClickListener {
            fab.isEnabled = false
            openCart()
        }

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
    }

    private fun loadActivityData() {
        user = realm.where(User::class.java).findFirst()
        if (user != null) {
            nav_view.getHeaderView(0).findViewById<TextView>(R.id.username).text = getString(R.string.username_text, user!!.firstName, user!!.lastName)
        }
        if (!pref.getBoolean(getString(R.string.FCM_REGISTERED), false)) {
            AppFirebaseIdService.registerDeviceToServer(pref.getString(getString(R.string.FCM_TOKEN), "")!!, applicationContext)
        }
        loadMainFragment()
    }

    private fun loadMainFragment() {
        supportFragmentManager.beginTransaction().replace(R.id.flContent, MainPagerFragment(), "FRAGMENT_MAIN").addToBackStack(null).commitAllowingStateLoss()
        supportActionBar!!.title = getString(R.string.home_title)
        nav_view.menu.getItem(0).isChecked = true
    }

    fun loadMealsFragment(type: Short, itemId: Long, title: String) {
        val fragment = MealListFragment()
        val args = Bundle()
        args.putShort("type", type)
        args.putLong("itemID", itemId)
        args.putString("title", title)
        fragment.arguments = args
        supportFragmentManager.beginTransaction().add(R.id.flContent, fragment, "FRAGMENT_MEALS").addToBackStack(null).commit()
    }

    fun logout() {
        val dlg = LogoutRequestDialog()
        dlg.show(supportFragmentManager, "logoutRequest")
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            val fragment = supportFragmentManager.findFragmentById(R.id.flContent)
            if (fragment is MainPagerFragment)
                finish()
            else if (fragment is MealListFragment) {
                supportFragmentManager.popBackStack()
                supportActionBar!!.title = getString(R.string.home_title)
            }
        }
    }

    fun openCart() {
        val intent = Intent(this, CartActivity::class.java)
        startActivityForResult(intent, RequestCodes.REQUEST_CART_OPEN)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        var fragment: Fragment? = null
        val title = getString(R.string.app_name)
        val name = "category"
        var changeFragment = false
        when (item.itemId) {
            R.id.nav_home -> {
                // Handle the camera action
                changeFragment = true
                fragment = MainPagerFragment.newInstance()
            }
            R.id.nav_my_cart -> {
                startActivity(Intent(this, CartActivity::class.java))
                nav_view.menu.getItem(1).isChecked = true
            }
            R.id.nav_order_history -> {
                startActivity(Intent(this, OrderHistoryActivity::class.java))
                nav_view.menu.getItem(2).isChecked = true
            }
            R.id.nav_telegram -> {

            }
            R.id.nav_logout -> {
                logout()
            }
        }
        if (changeFragment && fragment != null) {
            supportFragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack(name).commit()
            supportActionBar!!.title = title
        }

        drawer_layout.closeDrawer(GravityCompat.START)

        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RequestCodes.REQUEST_LOGIN) {
            if (resultCode != RequestCodes.LOGIN_SUCCESSFULL) {
                finish()
            } else {
                loadActivityData()
            }
        } else if (requestCode == RequestCodes.REQUEST_CART_OPEN) {
            fab.isEnabled = true
        }

    }
}
