package uz.diamondsolutions.sushikatanaapp

import android.app.Application
import io.realm.Realm
import io.realm.Realm.setDefaultConfiguration
import io.realm.RealmConfiguration
import uz.diamondsolutions.sushikatanaapp.models.User
import uk.co.chrisjenx.calligraphy.CalligraphyConfig




class SushiApp : Application() {

    public var user: User? = null

    override fun onCreate() {

        super.onCreate()
        Realm.init(this)
        val realmConfiguration = RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .schemaVersion(7)
                .build()
        Realm.setDefaultConfiguration(realmConfiguration)
        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Roboto-RobotoRegular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        )
    }

    override fun onTerminate() {
        Realm.getDefaultInstance().close()
        super.onTerminate()
    }

}