package uz.diamondsolutions.sushikatanaapp.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.Toast
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_cart.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import uz.diamondsolutions.sushikatana.retrofit.SushiApi
import uz.diamondsolutions.sushikatanaapp.R
import uz.diamondsolutions.sushikatanaapp.adapter.NewCartAdapter
import uz.diamondsolutions.sushikatanaapp.models.Cart
import uz.diamondsolutions.sushikatanaapp.models.Order
import uz.diamondsolutions.sushikatanaapp.models.User
import uz.diamondsolutions.sushikatanaapp.utils.RealmController
import uz.diamondsolutions.sushikatanaapp.utils.RequestCodes
import uz.diamondsolutions.sushikatanaapp.utils.ServiceGenerator
import uz.diamondsolutions.sushikatanaapp.utils.TextFormatters


class CartActivity : AppCompatActivity() {

    private lateinit var realm: Realm
    lateinit var adapter: NewCartAdapter
    lateinit var cart: Cart
    lateinit var token: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)

        setSupportActionBar(toolbar)
        toolbar.title = getString(R.string.cart_title)
        toolbar.setNavigationIcon(R.drawable.baseline_arrow_back_white_18)
        toolbar.setNavigationOnClickListener {
            finish()
        }


        realm = RealmController.with(this).realm
        try {
            cart = realm.where(Cart::class.java).findFirst()!!
        } catch (ex: KotlinNullPointerException) {
            realm.executeTransaction {
                cart = realm.createObject(Cart::class.java)
            }
        }

        cart_rv.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        adapter = NewCartAdapter(this)


        cart_rv.adapter = adapter
        refreshTotal()
        sendOrderBtn.setOnClickListener {
            startActivityForResult(Intent(this, OrderActivity::class.java), RequestCodes.REQUEST_ORDER_ACTIVITY)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RequestCodes.REQUEST_ORDER_ACTIVITY) {
            if (resultCode == RequestCodes.ORDER_SUCCESSFULL) {
                finish()
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    fun refreshTotal() {
        cart_total.text = TextFormatters.priceFormatter().format(cart.getTotal())
    }

    fun addCartItemCount(position: Int) {
        realm.executeTransaction {
            cart.items[position]!!.count += 1
        }
        adapter.notifyDataSetChanged()
        refreshTotal()
    }

    override fun onDestroy() {
        super.onDestroy()
        setResult(-1)
    }

    fun substractCartItem(position: Int) {
        if (cart.items[position]!!.count > 0) {
            realm.executeTransaction {
                cart.items[position]!!.count -= 1
            }
            adapter.notifyDataSetChanged()
            refreshTotal()
        }
    }

    fun removeCartItem(position: Int) {
        if (cart.items.size > position) {
            realm.executeTransaction {
                cart.items.remove(cart.items[position])
            }
            adapter.notifyDataSetChanged()
            refreshTotal()
        }
    }



    fun clearCart() {
        if (cart.items.size > 0) {
            realm.executeTransaction {
                cart.items.clear()
            }
            adapter.notifyDataSetChanged()
            refreshTotal()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.cart_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item!!.itemId
        if (id == R.id.cart_menu_clear)
            clearCart()
        return super.onOptionsItemSelected(item)
    }

}
