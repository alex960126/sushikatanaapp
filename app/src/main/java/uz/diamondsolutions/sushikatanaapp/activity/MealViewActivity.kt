package uz.diamondsolutions.sushikatanaapp.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.Button
import android.widget.TextView

import com.daimajia.slider.library.Animations.DescriptionAnimation
import com.daimajia.slider.library.SliderLayout
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.TextSliderView

import java.util.HashMap

import uz.diamondsolutions.sushikatanaapp.R
import uz.diamondsolutions.sushikatanaapp.models.Meal
import uz.diamondsolutions.sushikatanaapp.dialog.showCreateCategoryDialog


class MealViewActivity : AppCompatActivity() {

    private var sliderShow: SliderLayout? = null
    private var title: TextView? = null
    private var desc: TextView? = null
    private var price: TextView? = null
    private var ready: TextView? = null
    private var buy_button: Button? = null

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_meal_view)
        val toolbar = findViewById<Toolbar>(R.id.meal_view_toolbar)
        toolbar.setNavigationIcon(R.drawable.baseline_arrow_back_white_18)
        toolbar.setNavigationOnClickListener { finish() }

        title = findViewById(R.id.title_meal)
        desc = findViewById(R.id.description)
        price = findViewById(R.id.price)
        ready = findViewById(R.id.ready_time)

        val meal = intent.getSerializableExtra("meal") as Meal

        buy_button = findViewById(R.id.buy)
        if (meal != null) {
            title!!.text = meal.title
            desc!!.text = meal.description
            price!!.text = java.lang.Long.toString(meal.price)
            ready!!.text = meal.ready_time

        }

        buy_button!!.setOnClickListener { showCreateCategoryDialog(this@MealViewActivity, meal) }


        sliderShow = findViewById<View>(R.id.slider) as SliderLayout

        val file_maps = HashMap<String, Int>()
        file_maps["Sushi_1"] = R.drawable.sushi_1
        file_maps["Sushi_2"] = R.drawable.sushi_2
        file_maps["Sushi_3"] = R.drawable.sushi_3

        for (name in file_maps.keys) {
            val textSliderView = TextSliderView(this)
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(file_maps[name]!!).scaleType = BaseSliderView.ScaleType.Fit

            //add your extra information
            textSliderView.bundle(Bundle())
            textSliderView.bundle
                    .putString("extra", name)

            sliderShow!!.addSlider(textSliderView)
        }

        sliderShow!!.setPresetTransformer(SliderLayout.Transformer.Default)
        sliderShow!!.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom)
        sliderShow!!.setCustomAnimation(DescriptionAnimation())
        sliderShow!!.setDuration(4000)
    }

    override fun onStop() {
        sliderShow!!.stopAutoCycle()
        super.onStop()
    }
}
