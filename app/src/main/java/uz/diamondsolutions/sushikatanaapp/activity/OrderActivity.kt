package uz.diamondsolutions.sushikatanaapp.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_order.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import uz.diamondsolutions.sushikatana.retrofit.SushiApi
import uz.diamondsolutions.sushikatanaapp.R
import uz.diamondsolutions.sushikatanaapp.models.Cart
import uz.diamondsolutions.sushikatanaapp.models.Order
import uz.diamondsolutions.sushikatanaapp.models.User
import uz.diamondsolutions.sushikatanaapp.utils.RealmController
import uz.diamondsolutions.sushikatanaapp.utils.RequestCodes
import uz.diamondsolutions.sushikatanaapp.utils.ServiceGenerator

class OrderActivity : AppCompatActivity() {

    private lateinit var realm: Realm
    lateinit var cart: Cart
    lateinit var token: String
    lateinit var imm: InputMethodManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order)

        setSupportActionBar(toolbar)
        toolbar.title = getString(R.string.order_title)
        toolbar.setNavigationIcon(R.drawable.baseline_arrow_back_white_18)
        toolbar.setNavigationOnClickListener {
            finish()
        }

        realm = RealmController.with(this).realm
        try {
            cart = realm.where(Cart::class.java).findFirst()!!
        } catch (ex: KotlinNullPointerException) {
            realm.executeTransaction {
                cart = realm.createObject(Cart::class.java)
            }
        }
        val user = realm.where(User::class.java).findFirst()
        address.setText(if (user?.address != null) user.address else "")
        phone.setText(if (user?.phone != null) user.phone else "")



        imm = getSystemService(android.content.Context.INPUT_METHOD_SERVICE) as android.view.inputmethod.InputMethodManager

        sendOrderBtn.setOnClickListener {
            val order = Order()
            order.address = address.text.toString()
            order.phone = phone.text.toString()
            order.user = user!!.id
            order.meals = cart.items
            order.order_type = if (del_here.isChecked) 0 else if (del_with_me.isChecked) 1 else 2
            val orderObject = order.toPostObject()
            ServiceGenerator.createService(SushiApi::class.java, applicationContext).sendOrder(orderObject).enqueue(object : Callback<Order> {
                override fun onResponse(call: Call<Order>, response: Response<Order>) {
                    Snackbar.make(sendOrderBtn, getString(R.string.order_success), Snackbar.LENGTH_SHORT).show()
                    realm.executeTransaction {
                        cart.items.clear()
                    }
                    setResult(RequestCodes.ORDER_SUCCESSFULL)
                }

                override fun onFailure(call: Call<Order>, t: Throwable) {
                    Toast.makeText(applicationContext, t.message, Toast.LENGTH_SHORT).show()
                }
            })
        }
    }

    override fun onDestroy() {
        setResult(RequestCodes.ORDER_CANCELED)
        super.onDestroy()
    }
}
