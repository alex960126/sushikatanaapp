package uz.diamondsolutions.sushikatanaapp.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_order_history.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import uz.diamondsolutions.sushikatana.retrofit.SushiApi
import uz.diamondsolutions.sushikatanaapp.R
import uz.diamondsolutions.sushikatanaapp.SushiApp
import uz.diamondsolutions.sushikatanaapp.adapter.OrderAdapter
import uz.diamondsolutions.sushikatanaapp.models.Order
import uz.diamondsolutions.sushikatanaapp.utils.ServiceGenerator

class OrderHistoryActivity : AppCompatActivity() {

    lateinit var adapter: OrderAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_history)

        setSupportActionBar(toolbar)
        toolbar.setNavigationIcon(R.drawable.baseline_arrow_back_white_18)
        toolbar.setNavigationOnClickListener {
            finish()
        }

        adapter = OrderAdapter(this)
        orders_rv.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        orders_rv.adapter = adapter
        loadOrders()
    }

    fun loadOrders() {
        ServiceGenerator.createService(SushiApi::class.java).getOrders().enqueue(object : Callback<List<Order>> {
            override fun onFailure(call: Call<List<Order>>, t: Throwable) {
                Snackbar.make(orders_rv, getString(R.string.orders_load_error), Snackbar.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<List<Order>>, response: Response<List<Order>>) {
                adapter.updateOrderList(response.body()!!)
                adapter.notifyDataSetChanged()
            }

        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.order_history_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == R.id.refresh)
            loadOrders()
        return super.onOptionsItemSelected(item)
    }
}
