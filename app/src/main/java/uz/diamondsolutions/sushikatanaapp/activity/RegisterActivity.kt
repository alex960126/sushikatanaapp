package uz.diamondsolutions.sushikatanaapp.activity

import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.widget.Toast
import com.google.gson.Gson
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_register.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import uz.diamondsolutions.sushikatana.retrofit.SushiApi
import uz.diamondsolutions.sushikatanaapp.R
import uz.diamondsolutions.sushikatanaapp.models.User
import uz.diamondsolutions.sushikatanaapp.models.UserSignModel
import uz.diamondsolutions.sushikatanaapp.utils.RealmController
import uz.diamondsolutions.sushikatanaapp.utils.ServiceGenerator

class RegisterActivity : AppCompatActivity() {

    lateinit var pref: SharedPreferences
    lateinit var realm: Realm

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        setSupportActionBar(toolbar)
        toolbar.title = getString(R.string.register_title)
        toolbar.setNavigationIcon(R.drawable.baseline_arrow_back_white_18)
        toolbar.setNavigationOnClickListener {
            finish()
        }

        pref = PreferenceManager.getDefaultSharedPreferences(this)
        realm = RealmController.with(this).realm

        sign_in_btn.setOnClickListener { _ ->
            val fullname = fullname_eb.text.toString()
            val first_name = fullname.split(' ')[0]
            val last_name = if (fullname.split(' ').size > 1) fullname.split(' ')[1] else ""
            var user = UserSignModel()
            user.username = username_eb.text.toString()
            user.firstName = first_name
            user.lastName = last_name
            user.phone = phone_eb.text.toString()
            user.password1 = password_eb.text.toString()
            user.address = address_eb.text.toString()
            ServiceGenerator.createService(SushiApi::class.java).signUpUser(user).enqueue(object: Callback<User> {
                override fun onFailure(call: Call<User>, t: Throwable) {
                    Toast.makeText(applicationContext, getString(R.string.sign_up_error), Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(call: Call<User>, response: Response<User>) {
                    val gson = Gson()
                    //GsonBuilder().setPrettyPrinting().create()

                    pref.edit().putString("USER", gson.toJson(user)).putInt("USERID", user.id.toInt()).apply()
                    realm = Realm.getDefaultInstance()
                    realm.executeTransaction {
                        val rows = realm.where(User::class.java).findAll()
                        rows.deleteAllFromRealm()
                        val new_user = realm.copyToRealm(response.body()!!)
                    }
                }

            })

        }

    }
}
