//package uz.diamondsolutions.sushikatanaapp.adapter
//
//import android.content.Context
//import android.databinding.DataBindingUtil
//import android.support.v7.widget.RecyclerView
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import com.bumptech.glide.Glide
//import kotlinx.android.synthetic.main.item_cart.view.*
//import kotlinx.android.synthetic.main.item_meal.view.*
//import uz.diamondsolutions.sushikatanaapp.R
//import uz.diamondsolutions.sushikatanaapp.dialog.showCreateCategoryDialog
//import uz.diamondsolutions.sushikatanaapp.fragment.CartFragment
//import uz.diamondsolutions.sushikatanaapp.models.Meal
//import uz.diamondsolutions.sushikatanaapp.models.MealCartItem
//import uz.diamondsolutions.sushikatanaapp.utils.TextFormatters
//import android.databinding.ViewDataBinding
//import kotlinx.android.synthetic.main.item_group.view.*
//
//
//class BindingCartAdapter(val context: Context?, val fragment: CartFragment): RecyclerView.Adapter<BindingCartAdapter.DataBindViewHolder>() {
//    private val inflater: LayoutInflater = LayoutInflater.from(context)
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingCartAdapter.DataBindViewHolder {
//        val binding = DataBindingUtil.inflate<ViewDataBinding>(LayoutInflater.from(parent.context), R.layout.item_cart, parent, false)
//        return DataBindViewHolder(binding)
//    }
//
//    override fun onBindViewHolder(holder: BindingCartAdapter.DataBindViewHolder, position: Int) {
//        var viewDataBinding = holder.viewDataBinding
//        viewDataBinding.setVariable(BR.cartItem, fragment.cart.items[position])
////        holder.title.text = fragment.cart.items[position]!!.meal!!.title
////        holder.total.text = TextFormatters.priceFormatter().format(fragment.cart.items[position]!!.meal!!.price * fragment.cart.items[position]!!.count!!)
////        Glide.with(context!!).load(fragment.cart.items[position]!!.meal!!.image).into(holder.image)
////        holder.count.text = fragment.cart.items[position]!!.count.toString()
////        holder.addAmountBtn.setOnClickListener {
//////            showCreateCategoryDialog(context, fragment.cart.items[position])
////        }
////        holder.substractAmountBtn.setOnClickListener {
////
////        }
//    }
//
//    override fun getItemCount(): Int {
//        return fragment.cart.items.size
//    }
//
//    inner class DataBindViewHolder(val viewDataBinding: ViewDataBinding) : RecyclerView.ViewHolder(viewDataBinding.root) {
//
//        init {
//            this.viewDataBinding.executePendingBindings()
//        }
//    }
////
////    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
////        val title = view.cart_meal_title
////        val total = view.cart_meal_total
////        val count = view.meal_count
////        val image = view.cart_meal_image
////        val addAmountBtn = view.addAmountBtn
////        val substractAmountBtn = view.substractAmountBtn
////
////    }
//}