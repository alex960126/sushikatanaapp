package uz.diamondsolutions.sushikatanaapp.adapter

import android.app.Activity
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_group.view.*
import uz.diamondsolutions.sushikatana.utils.Constants
import uz.diamondsolutions.sushikatanaapp.MainActivity
import uz.diamondsolutions.sushikatanaapp.R
import uz.diamondsolutions.sushikatanaapp.models.MealCategory

class CategoryAdapter(val activity: Activity?): RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {
    private lateinit var categories:List<MealCategory>
    private val inflater: LayoutInflater = LayoutInflater.from(activity)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryAdapter.ViewHolder {
        val view = inflater.inflate(R.layout.item_group, parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: CategoryAdapter.ViewHolder, position: Int) {
        holder.title.text = categories[position].title
        Glide.with(activity!!).load(categories[position].image).into(holder.image)
        holder.holder.setOnClickListener {
            (activity as MainActivity).loadMealsFragment(Constants.BY_CATEGORY, categories[position].id, categories[position].title)
        }
    }

    override fun getItemCount(): Int {
        return if(::categories.isInitialized) categories.size else 0
    }

    fun updateCategoryList(catList:List<MealCategory>){
        if (!::categories.isInitialized)
            categories = ArrayList()
        this.categories = catList
        notifyDataSetChanged()
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val title = view.title
        val image = view.image
        val holder = view.holder
    }
}