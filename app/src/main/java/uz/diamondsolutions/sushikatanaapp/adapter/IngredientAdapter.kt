package uz.diamondsolutions.sushikatanaapp.adapter

import android.app.Activity
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_group.view.*
import uz.diamondsolutions.sushikatana.utils.Constants
import uz.diamondsolutions.sushikatanaapp.MainActivity
import uz.diamondsolutions.sushikatanaapp.R
import uz.diamondsolutions.sushikatanaapp.models.Ingredient

class IngredientAdapter(val activity: Activity?): RecyclerView.Adapter<IngredientAdapter.ViewHolder>() {
    private lateinit var ingredients:List<Ingredient>
    private val inflater: LayoutInflater = LayoutInflater.from(activity)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IngredientAdapter.ViewHolder {
        val view = inflater.inflate(R.layout.item_ingredient, parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: IngredientAdapter.ViewHolder, position: Int) {
        holder.title.text = ingredients[position].title
        Glide.with(activity!!).load(ingredients[position].image).into(holder.image)
        holder.holder.setOnClickListener {
            (activity as MainActivity).loadMealsFragment(Constants.BY_INGREDIENT, ingredients[position].id, ingredients[position].title)
        }
    }

    override fun getItemCount(): Int {
        return if(::ingredients.isInitialized) ingredients.size else 0
    }

    fun updateCategoryList(catList:List<Ingredient>){
        if (!::ingredients.isInitialized)
            ingredients = ArrayList()
        this.ingredients = catList
        notifyDataSetChanged()
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val title = view.title
        val image = view.image
        val holder = view.holder
    }
}