package uz.diamondsolutions.sushikatanaapp.adapter

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.app.FragmentStatePagerAdapter
import uz.diamondsolutions.sushikatanaapp.R
import uz.diamondsolutions.sushikatanaapp.fragment.CategoryFragment
import uz.diamondsolutions.sushikatanaapp.fragment.IngredientFragment
import uz.diamondsolutions.sushikatanaapp.fragment.MealListFragment

class MainPagerAdapter internal constructor(fm: FragmentManager, var context: Context) : FragmentStatePagerAdapter(fm) {

    var _fragments = ArrayList<Fragment>()


    override fun getItem(position: Int): Fragment {
        return _fragments[position]
    }

    fun add(fragment: Fragment) {
        this._fragments.add(fragment)
    }

    override fun getCount(): Int {
        return this._fragments.size
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> context.getString(R.string.category_title)
            1 -> context.getString(R.string.ingredient_title)
            else -> context.getString(R.string.all_meals_title)
        }
    }
}