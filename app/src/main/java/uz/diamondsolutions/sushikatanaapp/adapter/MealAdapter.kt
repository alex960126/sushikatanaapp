package uz.diamondsolutions.sushikatanaapp.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_meal.view.*
import uz.diamondsolutions.sushikatanaapp.R
import uz.diamondsolutions.sushikatanaapp.activity.MealViewActivity
import uz.diamondsolutions.sushikatanaapp.dialog.showCreateCategoryDialog
import uz.diamondsolutions.sushikatanaapp.models.Meal
import uz.diamondsolutions.sushikatanaapp.utils.TextFormatters

class MealAdapter(val context: Context?): RecyclerView.Adapter<MealAdapter.ViewHolder>() {
    private lateinit var meals:List<Meal>
    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MealAdapter.ViewHolder {
        val view = inflater.inflate(R.layout.item_meal, parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: MealAdapter.ViewHolder, position: Int) {
        holder.title.text = meals[position].title
        holder.description.text = meals[position].description
        holder.price.text = TextFormatters.priceFormatter().format(meals[position].price)
        Glide.with(context!!).load(meals[position].image).into(holder.image)
        holder.time.text  = context.getString(R.string.ready_time_text, meals[position].ready_time)
        holder.addToCartBtn.setOnClickListener {
            val intent:Intent =  Intent(context, MealViewActivity::class.java)
            intent.putExtra("meal", meals[position])
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return if(::meals.isInitialized) meals.size else 0
    }

    fun updateCategoryList(mealList:List<Meal>){
        if (!::meals.isInitialized)
            meals = ArrayList()
        this.meals = mealList
        notifyDataSetChanged()
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val title = view.meal_title
        val price = view.meal_price
        val time = view.meal_ready_time
        val image = view.meal_image
        val description = view.meal_short_desc
        val addToCartBtn = view.meal_holder

    }
}