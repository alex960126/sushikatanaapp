package uz.diamondsolutions.sushikatanaapp.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_cart.view.*
import uz.diamondsolutions.sushikatanaapp.R
import uz.diamondsolutions.sushikatanaapp.activity.CartActivity
import uz.diamondsolutions.sushikatanaapp.utils.TextFormatters

class NewCartAdapter(val activity: CartActivity?): RecyclerView.Adapter<NewCartAdapter.ViewHolder>() {
    private val inflater: LayoutInflater = LayoutInflater.from(activity)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewCartAdapter.ViewHolder {
        val view = inflater.inflate(R.layout.item_cart, parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: NewCartAdapter.ViewHolder, position: Int) {
        holder.title.text = activity!!.cart.items[position]!!.meal!!.title
        Glide.with(activity).load(activity.cart.items[position]!!.meal!!.image).into(holder.image)
        holder.count.text = activity.cart.items[position]!!.count.toInt().toString()
        holder.price.text = TextFormatters.priceFormatter().format(activity.cart.items[position]!!.meal!!.price)
        holder.substractAmountBtn.setOnClickListener {
            activity.substractCartItem(position)
        }
        holder.addAmountBtn.setOnClickListener {
            activity.addCartItemCount(position)
        }
        holder.removeBtn.setOnClickListener {
            activity.removeCartItem(position)
        }
    }

    override fun getItemCount(): Int {
        return activity!!.cart.items.size
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val title = view.cart_meal_title!!
        val count = view.meal_count!!
        val image = view.cart_meal_image!!
        val price = view.cart_meal_price!!
        val addAmountBtn = view.addAmountBtn!!
        val substractAmountBtn = view.substractAmountBtn!!
        val removeBtn = view.cart_remove_btn!!
    }
}