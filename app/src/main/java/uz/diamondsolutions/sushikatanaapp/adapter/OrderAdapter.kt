package uz.diamondsolutions.sushikatanaapp.adapter

import android.content.Context
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_order.view.*
import uz.diamondsolutions.sushikatana.utils.Constants
import uz.diamondsolutions.sushikatanaapp.R
import uz.diamondsolutions.sushikatanaapp.models.Order
import uz.diamondsolutions.sushikatanaapp.utils.OrdersDiffCallback
import uz.diamondsolutions.sushikatanaapp.utils.TextFormatters

class OrderAdapter(val context: Context?): RecyclerView.Adapter<OrderAdapter.ViewHolder>() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    var list: ArrayList<Order> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderAdapter.ViewHolder {
        val view = inflater.inflate(R.layout.item_order, parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: OrderAdapter.ViewHolder, position: Int) {
        holder.created.text = list[position].created
        holder.total.text = TextFormatters.priceFormatter().format(list[position].getTotal())
        holder.index.text = list[position].id.toString()
        holder.status.text = Constants.Companion.STATUS[list[position].status]

    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun updateOrderList(catList:List<Order>){
        val diffResult = DiffUtil.calculateDiff(OrdersDiffCallback(list, catList))
        list.clear()
        list.addAll(catList)
        diffResult.dispatchUpdatesTo(this)
//        this.list = catList
//        notifyDataSetChanged()
    }


    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val created = view.order_created
        val total = view.order_total
        val index = view.order_number
        val status = view.order_status

    }
}