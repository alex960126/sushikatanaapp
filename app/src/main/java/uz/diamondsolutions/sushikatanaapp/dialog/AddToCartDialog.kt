package uz.diamondsolutions.sushikatanaapp.dialog

import android.content.Context
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.widget.EditText
import android.widget.ImageButton
import uz.diamondsolutions.sushikatanaapp.R
import uz.diamondsolutions.sushikatanaapp.models.Meal
import uz.diamondsolutions.sushikatanaapp.models.MealCartItem
import uz.diamondsolutions.sushikatanaapp.utils.addToCart

fun showCreateCategoryDialog(context: Context, meal: Meal) {
    val builder = AlertDialog.Builder(context)
    builder.setTitle(context.getString(R.string.pick_count))
    builder.setCancelable(false)
    var layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    val view = layoutInflater.inflate(R.layout.add_to_cart_dialog, null)

    val categoryEditText = view.findViewById(R.id.meal_count) as EditText
    categoryEditText.clearFocus()
    view.findViewById<ImageButton>(R.id.addAmountBtn).setOnClickListener {
        if (categoryEditText.text.isEmpty())
            categoryEditText.setText("0")
        categoryEditText.setText((categoryEditText.text.toString().toInt() + 1).toString())
    }
    view.findViewById<ImageButton>(R.id.substractAmountBtn).setOnClickListener {
        if (categoryEditText.text.isEmpty())
            categoryEditText.setText("0")
        if (categoryEditText.text.toString().toInt() > 0)
            categoryEditText.setText((categoryEditText.text.toString().toInt() - 1).toString())
    }

    builder.setView(view)

    // set up the ok button
    builder.setPositiveButton(R.string.add_to_cart) { dialog, p1 ->
        val newCategory = categoryEditText.text
        var isValid = true
        if (newCategory.isBlank()) {
            categoryEditText.error = context.getString(R.string.validation_empty)
            isValid = false
        }

        if (isValid) {
            var item = MealCartItem()
            item.meal = meal
            item.count = categoryEditText.text.toString().toDouble()
            addToCart(item)
            dialog.dismiss()
        }
    }

    builder.setNegativeButton(R.string.cancel) { dialog, p1 ->
        dialog.cancel()
    }

    builder.show()
}

