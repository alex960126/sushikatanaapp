package uz.diamondsolutions.sushikatanaapp.fragment


import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridLayout
import kotlinx.android.synthetic.main.fragment_category.view.*
import retrofit2.Call
import retrofit2.Callback
import uz.diamondsolutions.sushikatana.retrofit.SushiApi

import uz.diamondsolutions.sushikatanaapp.R
import uz.diamondsolutions.sushikatanaapp.adapter.CategoryAdapter
import uz.diamondsolutions.sushikatanaapp.models.MealCategory
import uz.diamondsolutions.sushikatanaapp.utils.ServiceGenerator
import android.widget.Toast
import retrofit2.Response
import uz.diamondsolutions.sushikatanaapp.models.Meal


class CategoryFragment : Fragment() {

    lateinit var adapter: CategoryAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_category, container, false)
        adapter = CategoryAdapter(activity)
        view.category_rv.adapter = adapter
        view.category_rv.layoutManager = GridLayoutManager(activity, 2)
        loadData()
        return view
    }



    override fun onResume() {
        loadData()
        super.onResume()
    }

    var LOG_TAG = "CAT_FRG"

    private fun loadData() {
        ServiceGenerator.createService(SushiApi::class.java, activity!!.applicationContext).getCategories().enqueue(object : Callback<List<MealCategory>> {
            override fun onResponse(call: Call<List<MealCategory>>, response: Response<List<MealCategory>>) {
                adapter.updateCategoryList(response.body()!!)
                Log.d(LOG_TAG, "response " + response.body()!!.size)
            }
            override fun onFailure(call: Call<List<MealCategory>>, t: Throwable) {
                Toast.makeText(context, t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

    companion object {
        fun newInstance():CategoryFragment{
            return CategoryFragment()
        }
    }


}
