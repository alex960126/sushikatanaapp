package uz.diamondsolutions.sushikatanaapp.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_ingredient.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import uz.diamondsolutions.sushikatana.retrofit.SushiApi

import uz.diamondsolutions.sushikatanaapp.R
import uz.diamondsolutions.sushikatanaapp.adapter.IngredientAdapter
import uz.diamondsolutions.sushikatanaapp.models.Ingredient
import uz.diamondsolutions.sushikatanaapp.models.MealCategory
import uz.diamondsolutions.sushikatanaapp.utils.ServiceGenerator

/**
 * A simple [Fragment] subclass.
 *
 */
class IngredientFragment : Fragment() {

    lateinit var adapter: IngredientAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_ingredient, container, false)
        adapter = IngredientAdapter(activity)
        view.ingredients_rv.layoutManager = GridLayoutManager(activity, 2)
        view.ingredients_rv.adapter = adapter
        loadData()
        return view
    }



    override fun onResume() {
        loadData()
        super.onResume()
    }

    val LOG_TAG = "ING_FRG"

    fun loadData() {
        ServiceGenerator.createService(SushiApi::class.java, activity!!.applicationContext).getIngredients().enqueue(object : Callback<List<Ingredient>> {
            override fun onResponse(call: Call<List<Ingredient>>, response: Response<List<Ingredient>>) {
                adapter.updateCategoryList(response.body()!!)
                Log.d(LOG_TAG, "response " + response.body()!!.size)
            }
            override fun onFailure(call: Call<List<Ingredient>>, t: Throwable) {
                Toast.makeText(context, t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

    companion object {
        fun newInstance():IngredientFragment{
            return IngredientFragment()
        }
    }


}
