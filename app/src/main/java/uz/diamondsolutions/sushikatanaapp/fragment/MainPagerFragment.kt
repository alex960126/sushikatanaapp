package uz.diamondsolutions.sushikatanaapp.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_main_pager.view.*

import uz.diamondsolutions.sushikatanaapp.R
import uz.diamondsolutions.sushikatanaapp.adapter.MainPagerAdapter
import android.support.v4.view.ViewPager.OnPageChangeListener



/**
 * MainPager Fragment
 *
 */
class MainPagerFragment : Fragment() {

    lateinit var pagerAdapter: MainPagerAdapter
    var selTab = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_main_pager, container, false)
        pagerAdapter = MainPagerAdapter(activity!!.supportFragmentManager, context!!)
        pagerAdapter.add(CategoryFragment())
        pagerAdapter.add(IngredientFragment())
        pagerAdapter.add(MealListFragment())
        view.viewpager_main.adapter = pagerAdapter
        return view
    }

    private val pageChangeListener = object : OnPageChangeListener {


        override fun onPageSelected(newPosition: Int) {
            selTab = newPosition
        }

        override fun onPageScrolled(arg0: Int, arg1: Float, arg2: Int) {}

        override fun onPageScrollStateChanged(arg0: Int) {}
    }

    override fun onResume() {
        super.onResume()
    }

    companion object {
        fun newInstance(): MainPagerFragment {
            return MainPagerFragment()
        }
    }
}
