package uz.diamondsolutions.sushikatanaapp.fragment


import android.os.Bundle
import android.support.design.R.attr.layoutManager
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_meal_list.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import uz.diamondsolutions.sushikatana.retrofit.SushiApi
import uz.diamondsolutions.sushikatana.utils.Constants
import uz.diamondsolutions.sushikatanaapp.MainActivity

import uz.diamondsolutions.sushikatanaapp.R
import uz.diamondsolutions.sushikatanaapp.adapter.MealAdapter
import uz.diamondsolutions.sushikatanaapp.models.Meal
import uz.diamondsolutions.sushikatanaapp.utils.ServiceGenerator
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration



/**
 * A simple [Fragment] subclass.
 *
 */
class MealListFragment : Fragment() {
    lateinit var adapter: MealAdapter
    var type: Short = 3
    var itemId: Long = -1
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_meal_list, container, false)
        adapter = MealAdapter(context)
        view.meals_rv.adapter = adapter
        val layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)

        val dividerItemDecoration = DividerItemDecoration(
                view.meals_rv.context,
                layoutManager.orientation
        )

        dividerItemDecoration.setDrawable(
                ContextCompat.getDrawable(context!!, R.drawable.recycler_divider)!!
        )
        view.meals_rv.layoutManager = layoutManager
        view.meals_rv.addItemDecoration(dividerItemDecoration)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if (arguments != null) {
            type = arguments!!.getShort("type", Constants.BY_CATEGORY)
            itemId = arguments!!.getLong("itemID", -1)
            (activity as MainActivity).supportActionBar!!.title = arguments!!.getString("title", "Суши")
        }
        loadData(type, itemId)
    }



    override fun onResume() {
        loadData(type, itemId)
        super.onResume()
    }


    var LOG_TAG = "MEAL_FRG"

    private fun loadData(type: Short, itemId: Long) {
        val service = ServiceGenerator.createService(SushiApi::class.java, activity!!.applicationContext)

        val call = if (type == Constants.BY_CATEGORY)
            service.getMealsByCategory(itemId)
        else if (type == Constants.BY_INGREDIENT)
            service.getMealsByIngredients(itemId)
        else
            service.getMeals()
        call.enqueue(object : Callback<List<Meal>> {
            override fun onResponse(call: Call<List<Meal>>, response: Response<List<Meal>>) {
                adapter.updateCategoryList(response.body()!!)
            }

            override fun onFailure(call: Call<List<Meal>>, t: Throwable) {
                Toast.makeText(context, t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }
    companion object {
        fun newInstance():MealListFragment{
            return MealListFragment()
        }
    }
}
