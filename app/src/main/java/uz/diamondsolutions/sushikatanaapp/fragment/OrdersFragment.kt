package uz.diamondsolutions.sushikatanaapp.fragment

import android.content.Context
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_orders.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import uz.diamondsolutions.sushikatana.retrofit.SushiApi
import uz.diamondsolutions.sushikatanaapp.adapter.OrderAdapter

import uz.diamondsolutions.sushikatanaapp.R
import uz.diamondsolutions.sushikatanaapp.models.MealCategory
import uz.diamondsolutions.sushikatanaapp.models.Order
import uz.diamondsolutions.sushikatanaapp.utils.ServiceGenerator


class OrdersFragment : Fragment() {

    lateinit var adapter: OrderAdapter
    lateinit var pref: SharedPreferences
    lateinit var token: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.fragment_orders, container, false)
        view.orders_rv.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        adapter = OrderAdapter(context)
        view.orders_rv.adapter = adapter
        pref = PreferenceManager.getDefaultSharedPreferences(context)
        token = pref.getString("TOKEN", "")
        loadData()
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onDetach() {
        super.onDetach()
    }

    fun loadData() {

        ServiceGenerator.createService(SushiApi::class.java, activity!!.applicationContext).getOrders().enqueue(object : Callback<List<Order>> {
            override fun onResponse(call: Call<List<Order>>, response: Response<List<Order>>) {
                if (response.body() != null) {
                    adapter.updateOrderList(response.body()!!)
                }
            }

            override fun onFailure(call: Call<List<Order>>, t: Throwable) {
            }

        })
    }


    companion object {
        @JvmStatic
        fun newInstance() =
                OrdersFragment().apply {
                    //                    arguments = Bundle().apply {
//                        putString(ARG_PARAM1, param1)
//                        putString(ARG_PARAM2, param2)
//                    }
                }
    }
}
