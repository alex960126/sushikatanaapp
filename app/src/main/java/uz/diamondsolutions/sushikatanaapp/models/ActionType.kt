package uz.diamondsolutions.sushikatanaapp.models

object ActionType {
    val OPEN = "open"
    val MENU = "menu"
    val ADD_TO_CART = "add to cart"
}