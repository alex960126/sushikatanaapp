package uz.diamondsolutions.sushikatanaapp.models

import io.realm.RealmList
import io.realm.RealmObject
import android.databinding.Bindable

open class Cart(

        var items: RealmList<MealCartItem> = RealmList()

) : RealmObject() {
    fun getTotal(): Double {
        var total = 0.0
        for (item in items) {
            total += item.count!! * item.meal!!.price
        }
        return total
    }
}