package uz.diamondsolutions.sushikatanaapp.models

import com.google.gson.annotations.SerializedName

open class FcmDevice(
    @SerializedName("name") var name: String = "",
    @SerializedName("registration_id") var reg_id: String = "",
    @SerializedName("device_id") var dev_id: String = "",
    @SerializedName("active") var active: Boolean = true,
    @SerializedName("type") var type: String = "android"
)