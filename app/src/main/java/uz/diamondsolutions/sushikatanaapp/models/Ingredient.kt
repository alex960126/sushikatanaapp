package uz.diamondsolutions.sushikatanaapp.models

import com.google.gson.annotations.SerializedName

open class Ingredient(
        @SerializedName("id") var id: Long = -1,
        @SerializedName("title") var title: String,
        @SerializedName("meal_count") var meal_count: Long = 0,
        @SerializedName("image") var image: String? = null
)