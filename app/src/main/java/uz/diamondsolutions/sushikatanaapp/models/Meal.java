package uz.diamondsolutions.sushikatanaapp.models;

import android.databinding.BaseObservable;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

public class Meal extends RealmObject implements Serializable {
    @PrimaryKey
    private long id;

    @SerializedName("title")
    private String title;

    @SerializedName("price")
    private long price;
    @SerializedName("discounted_price")
    private long discounted_price;

    @SerializedName("description")
    private String description;
    @SerializedName("image")
    private String image;
    @SerializedName("ready_time")
    private String ready_time;

    @Ignore
    @Expose(serialize = false, deserialize = false)
    private double count = 0.0;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public long getDiscounted_price() {
        return discounted_price;
    }

    public void setDiscounted_price(long discounted_price) {
        this.discounted_price = discounted_price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getReady_time() {
        return ready_time;
    }

    public void setReady_time(String ready_time) {
        this.ready_time = ready_time;
    }
}
