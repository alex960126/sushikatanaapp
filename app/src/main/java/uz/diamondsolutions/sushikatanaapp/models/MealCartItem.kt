package uz.diamondsolutions.sushikatanaapp.models

import android.databinding.Bindable
import android.databinding.Observable
import android.databinding.PropertyChangeRegistry
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.Ignore
import io.realm.annotations.PrimaryKey
import uz.diamondsolutions.sushikatanaapp.utils.RealmDataBinding
import uz.diamondsolutions.sushikatanaapp.utils.TextFormatters

open class MealCartItem(
        @PrimaryKey @Expose(serialize = false, deserialize = false) var id: Long? = null,
        @get:Bindable @SerializedName("meal") var meal: Meal? = null,
        @get:Bindable @SerializedName("count") var count: Double = 0.0
) : RealmObject(), Observable, RealmDataBinding {

    @Ignore
    @Transient
    private var mCallbacks: PropertyChangeRegistry? = null

    @Ignore
    @Bindable
    var total: String? = null
        get() = TextFormatters.priceFormatter().format(meal!!.price * count)


    @Synchronized
    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        if (mCallbacks != null) {
            mCallbacks!!.remove(callback)
        }
    }

    @Synchronized
    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        if (mCallbacks == null) {
            mCallbacks = PropertyChangeRegistry()
        }
        mCallbacks!!.add(callback)
    }

    @Synchronized
    override fun notifyChange() {
        if (mCallbacks != null) {
            mCallbacks!!.notifyCallbacks(this, 0, null)
        }
    }

    fun notifyPropertyChanged(fieldId: Int) {
        if (mCallbacks != null) {
            mCallbacks!!.notifyCallbacks(this, fieldId, null)
        }
    }
}