package uz.diamondsolutions.sushikatanaapp.models

data class MealCartItemPostObject (
        var meal: Long? = null,
        var count: Double? = null
)