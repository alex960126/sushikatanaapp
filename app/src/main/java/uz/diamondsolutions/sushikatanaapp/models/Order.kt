package uz.diamondsolutions.sushikatanaapp.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject

open class Order(

        @SerializedName("id") var id: Long? = null,
        @SerializedName("created") var created: String? = null,
        @SerializedName("changed") var changed: String? = null,
        @SerializedName("status") var status: Int = 0,
        @SerializedName("address") var address: String? = null,
        @SerializedName("phone") var phone: String? = null,
        @SerializedName("user") var user: Long? = null,
        @SerializedName("order_type") var order_type: Int = 0,
        @SerializedName("meals") var meals: RealmList<MealCartItem> = RealmList()

) {
    fun toPostObject() : OrderPostObject {
        var postObject = OrderPostObject()
        for (item in meals) {
            postObject.meals.add(MealCartItemPostObject(item.meal!!.id, item.count))
        }
        postObject.address = address
        postObject.user = user
        postObject.phone = phone
        postObject.order_type = order_type
        return postObject
    }

    fun getTotal() : Double {
        var total = 0.0
        for (item in meals) {
            total += item.count * item.meal!!.price
        }
        return total
    }
}