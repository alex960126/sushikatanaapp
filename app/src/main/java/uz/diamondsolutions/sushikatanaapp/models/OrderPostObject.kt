package uz.diamondsolutions.sushikatanaapp.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class OrderPostObject(
        @Expose @SerializedName("meals") var meals: ArrayList<MealCartItemPostObject> = ArrayList(),
        @Expose @SerializedName("address") var address: String? = null,
        @Expose @SerializedName("phone") var phone: String? = null,
        @Expose @SerializedName("user") var user: Long? = null,
        @Expose @SerializedName("status") var status: Int = 0,
        @Expose @SerializedName("order_type") var order_type: Int = 0
)
