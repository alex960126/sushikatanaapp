//
//import android.databinding.Bindable
//import android.databinding.Observable.OnPropertyChangedCallback
//import android.databinding.PropertyChangeRegistry
//import io.realm.annotations.PrimaryKey
//import io.realm.RealmObject
//
//import android.databinding.Observable.OnPropertyChangedCallback
//import android.databinding.PropertyChangeRegistry
//
//import io.realm.RealmObject
//import io.realm.annotations.PrimaryKey
//
//class Post : RealmObject(), Observable, RealmDataBinding {
//    @PrimaryKey
//    @get:Bindable
//    // !isManaged() in Realm 2.0
//    var id: Long = 0
//        set(id) {
//            field = id
//            if (!isValid) {
//                notifyPropertyChanged(BR.id)
//            }
//        }
//
//    @get:Bindable
//    // !isManaged() in Realm 2.0
//    var text: String? = null
//        set(text) {
//            field = text
//            if (!isValid) {
//                notifyPropertyChanged(BR.text)
//            }
//        }
//
//    // FROM BASE OBSERVABLE
//    @Ignore
//    @Transient
//    private var mCallbacks: PropertyChangeRegistry? = null
//
//    // from observable interface
//    @Synchronized
//    fun addOnPropertyChangedCallback(callback: OnPropertyChangedCallback) {
//        if (mCallbacks == null) {
//            mCallbacks = PropertyChangeRegistry()
//        }
//        mCallbacks!!.add(callback)
//    }
//
//    // from observable interface
//    @Synchronized
//    fun removeOnPropertyChangedCallback(callback: OnPropertyChangedCallback) {
//        if (mCallbacks != null) {
//            mCallbacks!!.remove(callback)
//        }
//    }
//
//    /**
//     * Notifies listeners that all properties of this instance have changed.
//     * Inherited from RealmDataBinding interface (along with it coming from BaseObservable)
//     */
//    @Synchronized
//    fun notifyChange() {
//        if (mCallbacks != null) {
//            mCallbacks!!.notifyCallbacks(this, 0, null)
//        }
//    }
//
//    /**
//     * Notifies listeners that a specific property has changed. The getter for the property
//     * that changes should be marked with [Bindable] to generate a field in
//     * `BR` to be used as `fieldId`.
//     *
//     * @param fieldId The generated BR id for the Bindable field.
//     */
//    fun notifyPropertyChanged(fieldId: Int) {
//        if (mCallbacks != null) {
//            mCallbacks!!.notifyCallbacks(this, fieldId, null)
//        }
//    }
//}