package uz.diamondsolutions.sushikatana.models

import com.google.gson.annotations.SerializedName

public data class Token(
        @field:SerializedName("token")
        var token: String? = null,

        @field:SerializedName("non_field_errors")
        var non_field_errors: String? = null
)