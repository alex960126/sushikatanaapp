package uz.diamondsolutions.sushikatanaapp.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey


public open class User : RealmObject() {
    @PrimaryKey
    var id: Long = 0
    @SerializedName("username")
    var username: String? = null
    @SerializedName("email")
    var email: String? = null
    @SerializedName("first_name")
    var firstName: String? = null
    @SerializedName("last_name")
    var lastName: String? = null
    @SerializedName("phone")
    var phone: String? = null
    @SerializedName("address")
    var address: String? = null
}
