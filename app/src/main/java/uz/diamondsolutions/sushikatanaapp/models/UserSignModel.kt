package uz.diamondsolutions.sushikatanaapp.models

import com.google.gson.annotations.SerializedName
import io.realm.annotations.PrimaryKey

public open class UserSignModel {
    @PrimaryKey
    var id: Long = 0
    @SerializedName("username")
    var username: String? = null
    @SerializedName("email")
    var email: String? = null
    @SerializedName("first_name")
    var firstName: String? = null
    @SerializedName("last_name")
    var lastName: String? = null
    @SerializedName("phone")
    var phone: String? = null
    @SerializedName("password")
    var password1: String? = null
    @SerializedName("address")
    var address: String? = null
}
