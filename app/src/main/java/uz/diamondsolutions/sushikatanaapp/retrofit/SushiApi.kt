package uz.diamondsolutions.sushikatana.retrofit

import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.*
import uz.diamondsolutions.sushikatanaapp.models.*

interface SushiApi {
    @GET("/api/meals/")
    fun getMeals(): Call<List<Meal>>

    @GET("/api/orders/")
    fun getOrders(): Call<List<Order>>

    @GET("/api/meals/")
    fun getMealsByCategory(@Query("category") id: Long): Call<List<Meal>>

    @GET("/api/meals/")
    fun getMealsByIngredients(@Query("ingredient") id: Long): Call<List<Meal>>

    @GET("/api/categories/")
    fun getCategories(): Call<List<MealCategory>>

    @GET("/api/ingredients/")
    fun getIngredients(): Call<List<Ingredient>>

    @POST("/api/orders/")
    fun sendOrder(@Body order: OrderPostObject): Call<Order>

    @POST("/api/devices/")
    fun regDevice(@Body device: FcmDevice): Call<FcmDevice>

    @POST("/api/signup/")
    fun signUpUser(@Body user: UserSignModel): Call<User>



}