package uz.diamondsolutions.sushikatanaapp.retrofit;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import uz.diamondsolutions.sushikatana.models.Token;

/**
 * Created by bakha on 14.01.2018.
 */

public interface TokenRefresh {
    @Headers("Content-Type: application/json")
    @POST("/api-token-refresh/")
    Call<Token> getToken(@Body String body);
}
