package uz.diamondsolutions.sushikatanaapp.retrofit;

import retrofit2.Call;
import retrofit2.http.GET;
import uz.diamondsolutions.sushikatanaapp.models.User;
import uz.diamondsolutions.sushikatanaapp.models.UserCredentials;

/**
 * Created by bakha on 13.01.2018.
 */

public interface UserService {
    @GET("/api/me/")
    Call<User> me();


}