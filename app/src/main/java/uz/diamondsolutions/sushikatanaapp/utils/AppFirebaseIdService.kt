package uz.diamondsolutions.sushikatanaapp.utils

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.preference.PreferenceManager
import android.support.v4.app.NotificationCompat
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import uz.diamondsolutions.sushikatanaapp.MainActivity
import uz.diamondsolutions.sushikatanaapp.R
import uz.diamondsolutions.sushikatana.retrofit.SushiApi
import uz.diamondsolutions.sushikatana.utils.Constants
import uz.diamondsolutions.sushikatanaapp.models.FcmDevice


class AppFirebaseIdService : FirebaseMessagingService() {
    override fun onNewToken(p0: String?) {
        super.onNewToken(p0)
        Log.e("NEW_TOKEN", p0)
        val pref = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        val editor = pref.edit()
        editor.putString(getString(R.string.FCM_TOKEN), p0)
        editor.apply()

        val token = pref.getString(Constants.TOKEN, "")
        if (token!!.isEmpty()) {
            editor.putBoolean(getString(R.string.FCM_REGISTERED), false)
            editor.apply()
        } else {
            registerDeviceToServer(p0!!, applicationContext)
        }
    }


    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        val title = remoteMessage!!.getNotification()!!.title
        val message = remoteMessage.getNotification()!!.body
        Log.e("NEW_MESSAGE", remoteMessage.notification!!.body)


        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        val notificationBuilder = NotificationCompat.Builder(this, getString(R.string.notification_channel_id))
        notificationBuilder.setContentTitle(title)
        notificationBuilder.setContentText(message)
        notificationBuilder.setSmallIcon(R.mipmap.ic_launcher)
        notificationBuilder.setAutoCancel(true)
        notificationBuilder.color = Color.WHITE
        notificationBuilder.setContentIntent(pendingIntent)
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(0, notificationBuilder.build())
    }

    companion object {
        fun registerDeviceToServer(token: String, context: Context) {
            val fcmDevice = FcmDevice(
                    name = android.os.Build.MODEL,
                    reg_id = token,
                    dev_id = android.os.Build.ID
            )
            if (token != "") {
                ServiceGenerator.createService(SushiApi::class.java, context).regDevice(fcmDevice).enqueue(object : Callback<FcmDevice> {
                    override fun onResponse(call: Call<FcmDevice>, response: Response<FcmDevice>) {
                        Log.i("FCM", "Device register succes")
                        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(context.getString(R.string.FCM_REGISTERED), true).apply()
                    }

                    override fun onFailure(call: Call<FcmDevice>, t: Throwable) {
                        Log.e("FCM", "Device register error " + t.message)
                    }

                })
            } else {
                Log.e("FCM", "empty token")
            }
        }
    }
}

