package uz.diamondsolutions.sushikatana.utils

class Constants {
    companion object {
        const val SERVER_URL = "http://89.223.91.109"
//        const val SERVER_URL = "http://172.20.10.3:8000"
        const val TOKEN = "TOKEN"
        const val PREFS_NAME = "sushi_katana"
        const val BY_CATEGORY = 0.toShort()
        const val BY_INGREDIENT = 1.toShort()
        const val ALL_MEALS = 2.toShort()


        val STATUS = arrayListOf("В ожидании", "Подготовка", "Отправлено", "Доставлено", "Отменен")
    }
}