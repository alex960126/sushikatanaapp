package uz.diamondsolutions.sushikatanaapp.utils

import android.support.annotation.Nullable
import android.support.v7.util.DiffUtil
import uz.diamondsolutions.sushikatanaapp.models.Order


class OrdersDiffCallback(private var newOrders: List<Order>, private var oldOrders: List<Order>) : DiffUtil.Callback() {

    override fun getOldListSize(): Int {
        return oldOrders.size
    }

    override fun getNewListSize(): Int {
        return newOrders.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldOrders[oldItemPosition].id === newOrders[newItemPosition].id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldOrders[oldItemPosition] == newOrders[newItemPosition]
    }

    @Nullable
    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        //you can return particular field for changed item.
        return super.getChangePayload(oldItemPosition, newItemPosition)
    }
}