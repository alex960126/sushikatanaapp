package uz.diamondsolutions.sushikatanaapp.utils

import io.realm.RealmResults
import android.app.Activity
import android.app.Application
import android.support.v4.app.Fragment
import io.realm.Realm
import uz.diamondsolutions.sushikatanaapp.models.Order


class RealmController(application: Application) {
    val realm: Realm

//    //find all objects in the Book.class
//    val orders: RealmResults<Order>
//        get() = realm.where(Order::class.java).findAll()

    init {
        realm = Realm.getDefaultInstance()
    }

    //Refresh the realm istance
    fun refresh() {

        realm.refresh()
    }

//    //clear all objects from Book.class
//    fun clearAll() {
//
//        realm.beginTransaction()
//        realm.clear(Book::class.java)
//        realm.commitTransaction()
//    }

//    //query a single item with the given id
//    fun getBook(id: String): Book {
//
//        return realm.where(Book::class.java).equalTo("id", id).findFirst()
//    }


    companion object {

        var instance: RealmController? = null
            private set

        fun with(fragment: Fragment): RealmController {

            if (instance == null) {
                instance = RealmController(fragment.activity!!.application)
            }
            return instance as RealmController
        }

        fun with(activity: Activity): RealmController {

            if (instance == null) {
                instance = RealmController(activity.application)
            }
            return instance as RealmController
        }

        fun with(application: Application): RealmController {

            if (instance == null) {
                instance = RealmController(application)
            }
            return instance as RealmController
        }
    }
}