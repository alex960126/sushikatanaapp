package uz.diamondsolutions.sushikatanaapp.utils

import io.realm.RealmChangeListener


interface RealmDataBinding {
    interface Factory {
        fun create(): RealmChangeListener<*>
    }

    fun notifyChange()

    companion object {

        val FACTORY = {
            { element : RealmDataBinding.Factory ->
                if (element is RealmDataBinding) {
                    (element as RealmDataBinding).notifyChange()
                }
            }
        }
    }
}