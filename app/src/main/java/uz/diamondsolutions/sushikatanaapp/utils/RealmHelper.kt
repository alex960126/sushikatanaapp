package uz.diamondsolutions.sushikatanaapp.utils

import io.realm.Realm
import uz.diamondsolutions.sushikatanaapp.models.Cart
import uz.diamondsolutions.sushikatanaapp.models.MealCartItem

fun setUpRealm() {
}

fun addToCart(item: MealCartItem) {
    val realm = Realm.getDefaultInstance()
    var cart = realm.where(Cart::class.java)
            .findFirst()
    realm.executeTransaction {
        var nextId = 0L
        try {
            nextId = realm.where(MealCartItem::class.java).max("id")!!.toLong() + 1
        } catch (ex: KotlinNullPointerException) {
            nextId = 1
        }
        if (cart == null)
            cart = realm.createObject(Cart::class.java)
        else {
            var cItem: MealCartItem? = null
            var pos = -1
            var i = 0
            for (cart_item in cart!!.items) {
                i += 1
                if (cart_item.meal!!.id == item.meal!!.id) {
                    cItem = cart_item
                    break
                }
            }
            if (cItem != null) {
                cItem.count += item.count
//                cart!!.items[pos] = cItem
            } else {
                item.id = nextId
                cart!!.items.add(item)
            }
        }

    }

}