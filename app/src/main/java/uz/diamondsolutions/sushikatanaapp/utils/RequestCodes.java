package uz.diamondsolutions.sushikatanaapp.utils;

import org.jetbrains.annotations.NotNull;

/**
 * Created by bakha on 14.01.2018.
 */

public class RequestCodes {
    public static int REQUEST_LOGIN = 1;
    public static int REQUEST_SIGN_IN = 2;
    public static int REQUEST_CART_OPEN = 3;
    public static int REQUEST_ORDER_ACTIVITY = 4;
    public static int SIGNIN_SUCCESSFULL = 5;
    public static int SIGNIN_UNSUCCESSFULL = 6;
    public static int LOGIN_SUCCESSFULL = 3;
    public static int LOGIN_UNSUCCESSFULL = 4;

    public static int ORDER_SUCCESSFULL = 7;
    public static int ORDER_CANCELED = 8;
}