package uz.diamondsolutions.sushikatanaapp.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import uz.diamondsolutions.sushikatana.utils.Constants;

public class ServiceGenerator {

    private static Gson gson = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd HH:mm")
            .create();


    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(Constants.SERVER_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson));

    private static Retrofit retrofit = builder.build();

    public static <S> S createService(Class<S> serviceClass) {
        builder.client(httpClient.build());
        retrofit = builder.build();
        return retrofit.create(serviceClass);
    }



    public static <S> S createService(
            Class<S> serviceClass, Context mContext) {
        String token = PreferenceManager.getDefaultSharedPreferences(mContext).getString(Constants.TOKEN, "");
        if (!TextUtils.isEmpty(token)) {
            TokenManager manager = new TokenManager(mContext);
            AuthenticationInterceptor interceptor =
                    new AuthenticationInterceptor(manager);

            if (!httpClient.interceptors().contains(interceptor)) {
                httpClient.addInterceptor(interceptor);
                httpClient.connectTimeout(15, TimeUnit.SECONDS);
                httpClient.readTimeout(30, TimeUnit.SECONDS);

                builder.client(httpClient.build());
                retrofit = builder.build();
            }
        }

        return retrofit.create(serviceClass);
    }

    public static class AuthenticationInterceptor implements Interceptor {

        private TokenManager mTokenManager;

        public AuthenticationInterceptor(TokenManager mTokenManager) {
            this.mTokenManager = mTokenManager;
        }

        @Override
        public Response intercept(@NonNull Chain chain) throws IOException {
            Request original = chain.request();

            Request.Builder builder = original.newBuilder()
                    .header("Authorization",  "JWT " + mTokenManager.getToken());

            Request request = builder.build();
            Response response = chain.proceed(request);
            boolean unauthorized = response.code() == 401;
            if (unauthorized) {
                String newToken = mTokenManager.refreshToken();
                request = request.newBuilder()
                        .addHeader("Authorization", "JWT " + newToken)
                        .build();
                return chain.proceed(request);
            }
            return response;
        }
    }
}