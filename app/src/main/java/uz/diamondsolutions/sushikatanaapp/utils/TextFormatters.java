package uz.diamondsolutions.sushikatanaapp.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class TextFormatters {
    public static DecimalFormat priceFormatter() {
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setGroupingSeparator(' ');
        symbols.setDecimalSeparator(',');

        return new DecimalFormat("#,### сум", symbols);
    }
}
